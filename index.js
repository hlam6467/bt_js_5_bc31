// bài tập 1
// input :  nhập điểm 3 môn của thí sinh
// nhập khu vực ưu tiên
// nhập điểm chuẩn của hội đồng
//  cách xử lý:
//  nhập điểm chuẩn của hội đồng
//  tính tổng điểm của thí sinh + với điểm khu vực + điểm ưu tiên so sánh nếu cao hơn hoặc bằng điểm chuẩn thì là đậu còn không là trượt
// output : tổng điểm của thí sinh
document.getElementById("txt-ket-qua").addEventListener("click", function () {
  var diemChuan = document.getElementById("txt-diem-chuan").value * 1;
  var diemThu1 = document.getElementById("txt-diem-1").value * 1;
  var diemThu2 = document.getElementById("txt-diem-2").value * 1;
  var diemThu3 = document.getElementById("txt-diem-3").value * 1;
  var khuVuc1 = "kv1";
  var khuVuc2 = "kv2";
  var khuVuc3 = "kv3";
  var khuVuc4 = "kv4";
  var khuVucUuTien = function (chonKhuVuc) {
    switch (chonKhuVuc) {
      case khuVuc1:
        return 2;
      case khuVuc2:
        return 1;
      case khuVuc3:
        return 0.5;
      case khuVuc4:
        return 0;
      default:
        return 0;
    }
  };
  var KhuVuc = document.querySelector('input[name="optradio1"]:checked').value;
  var diemKhuVuc = khuVucUuTien(KhuVuc);

  var doiTuong1 = "doi-tuong-1";
  var doiTuong2 = "doi-tuong-2";
  var doiTuong3 = "doi-tuong-3";
  var doiTuong4 = "doi-tuong-4";
  var doiTuongUuTien = function (chonDoiTuong) {
    switch (chonDoiTuong) {
      case doiTuong1:
        return 2.5;
      case doiTuong2:
        return 1.5;
      case doiTuong3:
        return 1;
      case doiTuong4:
        return 0;
      default:
        return 0;
    }
  };
  var doiTuong = document.querySelector('input[name="optradio"]:checked').value;
  var diemDoiTuong = doiTuongUuTien(doiTuong);

  var tongDiem =
    (diemThu1 + diemThu2 + diemThu3 + diemKhuVuc + diemDoiTuong) * 1;
  if (tongDiem >= diemChuan) {
    document.getElementById(
      "ket-qua"
    ).innerHTML = `<div>Chúc mừng bạn đã đậu - Tổng số điểm của bạn là : ${tongDiem}</div>`;
  } else {
    document.getElementById(
      "ket-qua"
    ).innerHTML = `<div>Bạn đã trượt - Tổng số điểm của bạn là : ${tongDiem}</div>`;
  }
});
// Bài tập 2
// input : nhập tên và số điện tiêu thụ
// cách xử lý: tính giá tiền tùy thuộc vào số tiền điện tiêu thụ. ví dụ 60kw thì 50kw đầu tính giá 500d/1kw 10kw sau tính 650d/1kw
// output: tính tổng số tiền cần thanh toán
document.getElementById("txt-tien-dien").addEventListener("click", function () {
  var tenNguoi = document.getElementById("txt-ten").value;
  var soKw = document.getElementById("txt-so-kw").value * 1;
  const giaTienKwDau = 500;
  const giaTien50KwKe = 650;
  const giaTien100KwKe = 850;
  const giaTien150KwKe = 1100;
  const giaTienConLai = 1300;
  if (soKw <= 50) {
    tienDien = soKw * giaTienKwDau;
  } else if (soKw <= 100) {
    tienDien = (soKw - 50) * giaTien50KwKe + 50 * giaTienKwDau;
  } else if (soKw <= 200) {
    tienDien =
      (soKw - 100) * giaTien100KwKe + 50 * giaTienKwDau + 50 * giaTien50KwKe;
  } else if (soKw <= 350) {
    tienDien =
      (soKw - 200) * giaTien150KwKe +
      100 * giaTien100KwKe +
      50 * giaTienKwDau +
      50 * giaTien50KwKe;
  } else {
    tienDien =
      (soKw - 350) * giaTienConLai +
      150 * giaTien150KwKe +
      100 * giaTien100KwKe +
      50 * giaTienKwDau +
      50 * giaTien50KwKe;
  }
  document.getElementById(
    "txt-xac-nhan"
  ).innerHTML = `<div>Chào ${tenNguoi} - Tổng số tiền điện là ${tienDien}</div>`;
});
